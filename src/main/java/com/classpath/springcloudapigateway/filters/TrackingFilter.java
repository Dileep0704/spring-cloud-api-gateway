package com.classpath.springcloudapigateway.filters;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Component
public class TrackingFilter implements GlobalFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        exchange.getRequest().mutate().header("request_id", UUID.randomUUID().toString());

        System.out.println("Inside the Filter :::: ");

        System.out.println("Request Id passed into the header ::: "+ exchange.getRequest().getHeaders().get("request_id"));
        return chain.filter(exchange);
    }
}